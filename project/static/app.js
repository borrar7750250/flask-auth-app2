var resultsContainer = document.getElementById('resultsContainer');

function obtenerDatos(){
    let res_usuario = document.getElementById("buscador").value;
    const script = document.createElement('script');
    script.src = `https://api.flickr.com/services/feeds/photos_public.gne?tags=${res_usuario}&format=json&jsoncallback=processData`;
    document.body.appendChild(script);
}

function processData(data){
    console.log(data);
    if (resultsContainer.classList.contains('d-none')){
        resultsContainer.classList.remove('d-none');
        resultsContainer.classList.add('d-flex');
        resultsContainer.classList.add('flex-column');
    }

    while (resultsContainer.firstChild){
        resultsContainer.remove(resultsContainer.firstChild);
    }

    if (data.items.length > 0){
        data.items.forEach(element => {
            let img = document.createElement('img');
            img.src = element.media.m;
            resultsContainer.appendChild(img);
        });
    }
}
